package br.com.garcia.springboot.h2config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import org.apache.log4j.Logger;

public class ConnectionH2 {

	private static Logger logg = Logger.getLogger(ConnectionH2.class);
	
	public static Connection getConnection(){

		Connection conn = null;
		
		try{
			Class.forName("org.h2.Driver");
			conn = DriverManager.getConnection("jdbc:h2:mem:schema", "sa", "");
			Statement stmt = conn.createStatement();
			String insert = "CREATE TABLE PAIS( " +
					"ID_PAIS INT PRIMARY KEY, " +
					"NM_PAIS VARCHAR(100)); " +
					"INSERT INTO PAIS(ID_PAIS, NM_PAIS) VALUES(1, 'BRASIL'); " +
					"INSERT INTO PAIS(ID_PAIS, NM_PAIS) VALUES(2, 'COLOMBIA'); " +
					"INSERT INTO PAIS(ID_PAIS, NM_PAIS) VALUES(3, 'CHILE'); ";
			stmt.execute(insert);
			logg.info("CONNECTED");
			
		}catch(Exception ex){
			logg.error("ERROR");
		}
		
		return conn;
	}
	
	
	
}
