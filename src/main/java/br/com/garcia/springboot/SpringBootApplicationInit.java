package br.com.garcia.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SpringBootApplicationInit {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootApplicationInit.class, args);
	}

}
