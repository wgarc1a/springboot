package br.com.garcia.springboot.schedule;

import java.util.Date;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
@EnableScheduling
public class ScheduleConfig {
	
	@Scheduled(cron="0 35 9 * * *")
	public void atualizaData(){
	
		System.out.println(new Date());
		
	}

}
