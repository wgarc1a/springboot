package br.com.garcia.springboot.controller;


import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.garcia.springboot.beans.User;
import br.com.garcia.springboot.dao.UserDao;



@Controller
public class BaseController {

	private static Logger logg = Logger.getLogger(BaseController.class);
	private static final String INDEX = "index";
	
	@Resource
	private UserDao userDao;
	
	@RequestMapping(value = {"/", "/index"}, method = RequestMethod.GET)
	public String home(ModelMap modelMap){
		
		User user = new User();
		user.setId(1L);
		user.setNome("teste testando");
		user.setLogin("teste");
		user.setSenha("senha");
		user.setEmail("teste@gps.com.br");
		
		userDao.save(user);
		
		modelMap.addAttribute("paises", null);
		
        return INDEX;
    }
}
