package br.com.garcia.springboot.dao;

import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Repository;

@Repository(value = "genericDAO")
public interface Dao<T, PK extends Serializable> {

    public T getById(PK pk); 
    
    public void save(T entity);
 
    public void update(T entity);
 
    public void delete(T entity);
 
    public List<T> findAll();
 
	
}
