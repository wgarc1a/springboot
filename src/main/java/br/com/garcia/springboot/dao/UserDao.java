package br.com.garcia.springboot.dao;

import br.com.garcia.springboot.beans.User;

public interface UserDao extends Dao<User, Long> {

}
