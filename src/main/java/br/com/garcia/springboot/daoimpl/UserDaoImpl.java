package br.com.garcia.springboot.daoimpl;

import javax.persistence.EntityManager;

import br.com.garcia.springboot.beans.User;
import br.com.garcia.springboot.dao.UserDao;

public class UserDaoImpl extends GenericDAO<User, Long> implements UserDao{

	public UserDaoImpl(EntityManager entityManager) {
		super(entityManager);
	}

}
